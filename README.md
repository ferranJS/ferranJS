[![SVG Banners](https://svg-banners.vercel.app/api?type=rainbow&text1=I'm%20not%20a%20library!&width=519&height=140)](https://github.com/ferranJS/ferranJS)


[![typescript](https://skillicons.dev/icons?i=typescript)](https://github.com/standard/ts-standard)
[![angular](https://skillicons.dev/icons?i=angular)](https://github.com/angular)
[![nextjs](https://skillicons.dev/icons?i=nextjs)](https://github.com/vercel/next.js)
[![reactivex](https://skillicons.dev/icons?i=reactivex)](https://ngrx.io/)
[![firebase](https://skillicons.dev/icons?i=firebase)](https://firebase.google.com/)
[![tailwind](https://skillicons.dev/icons?i=tailwind)](https://github.com/tailwindlabs/tailwindcss)
[![github](https://skillicons.dev/icons?i=github)](https://docs.github.com/en)
[![gitlab](https://skillicons.dev/icons?i=gitlab)](https://about.gitlab.com/)
[![figma](https://skillicons.dev/icons?i=figma)](https://www.figma.com/)
[![regex](https://skillicons.dev/icons?i=regex)](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular)

<!--
https://github.com/tandpfun/skill-icons#icons-list
**ferranJS/ferranJS** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
